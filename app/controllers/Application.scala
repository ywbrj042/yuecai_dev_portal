package controllers

import play.api._
import play.api.mvc._
import models.Link

object Application extends Controller {
  
  def index = Action {
    val links = Link.findAll;
    Ok(views.html.index(links));
  }
  
}