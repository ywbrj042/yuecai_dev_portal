package models

/**
 * 悦采研发常用在线工具链接的模型。
 */
case class Link(link:String, name:String, userName:String, password:String, detail:String);

/**
 * 悦采研发在线工具类型模型。
 */
case class LinkType(name:String, links:List[Link]);

object Link{
  val linkTypes = List(
	  LinkType("在线协作工具", List(
	      Link("https://tower.bidlink.cn", "tower团队协作工具", "", "", "必联tower在线团队协作工具"),
	      Link("http://192.168.1.122/dashboard.action", "Wiki在线文档", "", "", "必联在线文档服务Wiki")
	  )),
	  LinkType("在线开发工具", List(
	      Link("http://repo.bidlink.cn/nexus/index.html", "nexus的maven包仓库", "", "", "必联maven包的私有仓库，包括snapshot和release各种版本的jar包"),
	      Link("svn.bidlink.cn/svn/yuecai", "svn源代码仓库", "", "", "必联的svn源代码仓库地址，该地址是悦采的源代码svn库地址"),
	      Link("http://fisheye.bidlink.cn/browse/yuecai", "FishEye查看悦采源代码/", "wubingyang", "wubingyang", "悦采查看源代码的web在线工具-FishEye"),
	      Link("http://172.16.0.200:8080/", "dubbo管理系统（开发环境）", "root", "root", "dubbo管理工具开发环境"),
	      Link("http://172.16.0.200:9080/", "Jenkins（开发环境）", "", "", "悦采开发环境的开源持续集成工具Jenkis在线服务地址"),
	      Link("http://www.bejson.com/", "Json在线格式化工具", "", "", "")
	  )),
	  LinkType("在线测试工具", List(
	      Link("http://jktest.bidlink.cn/view/%E6%82%A6%E9%87%87/", "Jenkins（测试环境）", "", "", "悦采测试环境的开源持续集成工具Jenkis在线服务地址"),
	      Link("http://test.yuecai.com", "悦采主干（测试环境）", "testcg01", "test1234", "悦采主干代码的部署测试环境网址"),
	      Link("http://iteration.yuecai.com", "悦采分支（测试环境）", "testcg01", "test1234", "悦采每周迭代的分支代码部署测试环境网址"),
	      Link("http://10.1.1.50:8080/config-manager/zookeeper/page/main.do", "统一配置管理（测试环境）", "admin", "admin", "必联统一配置管理工具，可以在该系统中查看开发、测试环境的各种配置信息，包括数据库配置、接口配置、业务参数配置等。"),
	      Link("http://10.1.1.62:8080/", "dubbo管理系统（测试环境）", "guest", "guest", "中心库接口和其它通用服务接口的管理中心"),
	      Link("http://10.1.1.60:8088/", "dubbo管理系统（工作流平台测试环境）", "guest", "guest", "工作流平台的远程接口管理中心"),
	      Link("http://192.168.0.16:8080/monitor-center/", "监控系统（测试环境）", "", "", "检测系统的运行状况，可以记录系统的内部运行状态，包括SQL语句执行时间，方法调用时间，服务状态，短信、缓存等信息状态，用户检测系统的运行情况及系统优化。"),
	      Link("http://10.1.1.155:8080/drake-center/", "drake工作流平台（测试环境）", "enquiry", "123456", "必联工作流平台在线管理工具测试环境地址"),
	      Link("http://nbmstest.app.bidlink.cn/nbms-mscenter/", "消息管理中心(测试环境，短信、邮件、RTX、商麦等)", "yuecai", "yuecai", "消息管理中心测试环境地址")
	  )),
	  LinkType("在线质量管理工具", List(
	      Link("http://sonar.bidlink.cn/", "SonarQube质量管理平台", "", "", "必联SonarQube质量管理平台"),
	      Link("http://pms.ebnew.com/", "禅道BUG管理工具", "", "", "必联禅道BUG在线管理工具")
	  )),
	  LinkType("在线部署和运维", List(
          Link("http://211.151.182.216:8181/governance/applications", "dubbo管理系统（中心库生产环境）", "guest", "bid123456", "可以在这里管理通过dubbo发布的rmi，dubbo协议的各种接口，包括中心库等通用接口"),
          Link("http://211.151.182.228:8088/governance/applications", "dubbo管理系统（工作流生产环境）", "guest", "guest", "可以在这里管理通过dubbo发布的rmi，dubbo协议的各种接口，包括工作流平台等服务的接口"),
          Link("http://211.151.182.228:8184/config-manager/zookeeper/page/main.do", "统一配置管理（生产环境）", "root", "******", "必联统一配置管理工具，可以在该系统中查看开发、测试环境的各种配置信息，包括数据库配置、接口配置、业务参数配置等。"),
	      Link("http://jk.bidlink.cn/view/yuecai/", "Jenkins（生产环境）", "", "", "悦采生产环境的开源持续集成工具Jenkis在线服务地址"),
	      Link("http://10.0.0.206:8080/drake-center/", "drake工作流平台（生产环境）", "enquiry", "123456", "必联工作流平台在线管理工具生产环境地址"),
	      Link("http://yuecai.com", "悦采（生产环境）", "", "*", "悦采正式提供给用户使用的生产环境网址"),
	      Link("http://211.151.182.228:8098/monitor-center/", "监控系统（生产环境）", "yangwubing", "****", ""),
	      Link("http://nbms.ebnew.com/nbms-mscenter/", "消息管理中心(生产环境，短信、邮件、RTX、商麦等)", "yuecai", "yuecai", "消息管理中心生产环境地址")
	  ))
  );
  
  /**
   * 查询所有超级连接。
   */
  def findAll = linkTypes;
}